//
//  JourneyPlannerVC.swift
//  DMRC
//
//  Created by Khushboo Almaula on 27/11/20.
//  Copyright © 2020 khushboo. All rights reserved.
//

import UIKit
import Toast_Swift

class JourneyPlannerVC: UIViewController, UIPickerViewDelegate,UIPickerViewDataSource {
    
    //MARK: IBOutlets and Variables
    
    @IBOutlet weak var txtStartJourney: UITextField!
    @IBOutlet weak var txtEndjourney: UITextField!
    @IBOutlet weak var lblTotalPrice: UILabel!
    @IBOutlet weak var btnConfirmBooking: UIButton!
    
    let arrStationName = JourneyDetail.arrJourneyDetail
    var startIndex = 0
    var endIndex = 0
    var arrStations = [[String:Any]]()
    var subStationIDStart = 0
    var subStationIDEnd = 0
    let picker = UIPickerView()
    var totalPrice = 0.0
    
    
    //MARK: ViewLife Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        txtStartJourney.inputView = picker
        txtEndjourney.inputView = picker
        picker.dataSource = self
        picker.delegate = self
        addToolbar()
        
        for obj in JourneyDetail.arrJourneyDetail{
            arrStations.append(obj)
            let arrSubstation = obj["subStations"] as? [[String:Any]] ?? [[String:Any]]()
            if arrSubstation.count != 0 {
                for i in arrSubstation{
                    arrStations.append(i)
                }
            }
        }
        // Do any additional setup after loading the view.
    }
    
    //Mark: UIPickerView Delegate and Datasource
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return arrStations.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String?{
        return arrStations[row]["stationName"] as? String
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int){
        if txtStartJourney.isEditing{
            self.txtStartJourney.text = arrStations[row]["stationName"] as? String ?? "nil"
        }
        else{
            self.txtEndjourney.text = arrStations[row]["stationName"] as? String ?? "nil"
        }
    }
    
    //MARK: IBAction Methods
    @IBAction func btnActionStartJourney(_ sender: Any) {
      
        if txtEndjourney.text == ""{
            self.view.makeToast("Please select destination")
        }
        else if txtStartJourney.text == ""{
            self.view.makeToast("Please select source")
        }
        else{
            self.calculatePrice()
        }
    }
    
    @IBAction func btnActionConfirmBooking(_sender: Any){
        if txtEndjourney.text == ""{
            self.view.makeToast("Please select destination")
        }
        else if txtStartJourney.text == ""{
            self.view.makeToast("Please select source")
        }
        else{
            self.performSegue(withIdentifier: "ConfirmbookingSegue", sender: self)
        }
    }
    
    //MARK: Custom Methods
    func calculatePrice(){
        print("Start journey")
        let fare  = 0.0
        totalPrice = 0.0
        var skipStation: Bool = false
        var countFirst: Bool = false
        
        if(startIndex > endIndex)
        {
            let temp = endIndex
            let subTemp = subStationIDEnd
            
            endIndex = startIndex
            startIndex = temp
            
            subStationIDEnd = subStationIDStart
            subStationIDStart = subTemp
            
            countFirst = true
        }
        
        if(startIndex != endIndex){
            skipStation = true
        }
        
        for i in startIndex...endIndex{
            print("stationName: \(String(describing: JourneyDetail.arrJourneyDetail[i]["stationName"]))")
            
            let fare = JourneyDetail.arrJourneyDetail[i]["totalPrice"] as? Double ?? 0.0
            
            if(i != startIndex){
                totalPrice = totalPrice + fare
            }
            
            //If the station has substations
            
            if(i == endIndex){
                //get all substations
                let subStations =  JourneyDetail.arrJourneyDetail[endIndex]["subStations"] as? [[String:Any]] ?? [[String:Any]]()
                
                // if subStations.count > 0 && subStationIDEnd != 0
                if subStations.count > 0 {
                    var sameLine : Bool = false
                    
                    if(startIndex == endIndex){
                        sameLine = true
                    }
                    
                    if(subStationIDStart > subStationIDEnd){
                        let subTemp = subStationIDEnd;
                        subStationIDEnd = subStationIDStart
                        subStationIDStart = subTemp
                    }
                    for j in subStationIDStart...subStationIDEnd{
                        if(j != subStationIDStart)
                        {
                            if(j == 0){
                                if(sameLine == true){
                                    totalPrice = totalPrice + 5
                                }
                            }
                            else{
                                let subfare = findSubStationFare(stations: subStations,id: j)
                                totalPrice = totalPrice + subfare
                            }
                        }
                    }
                }
            }
            
            lblTotalPrice.text = "Total Price from \(txtStartJourney.text!) to \(txtEndjourney.text!) is \(totalPrice)"
            self.btnConfirmBooking.setTitle("Confirm price \(totalPrice)", for: .normal)
        }
    }
    //Add toolbar on the top of picker
    func addToolbar(){
        let toolBar = UIToolbar()
        toolBar.barStyle = UIBarStyle.default
        toolBar.isTranslucent = true
        toolBar.tintColor = UIColor(red: 0/255, green: 132/255, blue: 151/255, alpha: 1)
        toolBar.sizeToFit()
        
        let doneButton = UIBarButtonItem(title: "Done", style: UIBarButtonItem.Style.plain, target: self, action: #selector(donePicker))
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "Cancel", style: UIBarButtonItem.Style.plain, target: self, action: #selector(cancelPicker))
        
        toolBar.setItems([cancelButton, spaceButton, doneButton], animated: false)
        toolBar.isUserInteractionEnabled = true
        
        txtStartJourney.inputAccessoryView = toolBar
        txtEndjourney.inputAccessoryView = toolBar
    }
    
    //find value of fare from the index
    func findSubStationFare(stations:[[String:Any]], id:Int) -> Double{
        if(stations != nil){
            for station in stations{
                let abc = station["subStationID"] as? Int ?? 0
                if(abc == id){
                    return station["totalPrice"] as? Double ?? 0.0
                }
            }
        }
        return 0.0
    }
    
    //Objective C method
    //Done button pressed from the picker
    @objc func donePicker() {
        let row = self.picker.selectedRow(inComponent: 0)
        
        if txtStartJourney.isEditing{
            txtStartJourney.resignFirstResponder()
            startIndex =  arrStations[row]["stationId"] as? Int ?? 0
            subStationIDStart = arrStations[row]["subStationID"] as? Int ?? 0
            
        }
        else{
            txtEndjourney.resignFirstResponder()
            endIndex = arrStations[row]["stationId"] as? Int ?? 0
            subStationIDEnd = arrStations[row]["subStationID"] as? Int ?? 0
        }
    }
    
    //Cancel button pressed
    @objc func cancelPicker() {
        txtStartJourney.resignFirstResponder()
        txtEndjourney.resignFirstResponder()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "ConfirmbookingSegue" {
            let confirmBooking = segue.destination as! ConfirmbookingVC
            confirmBooking.arrSelectedStation =
                [arrStations[startIndex],
                 arrStations[endIndex]]
            confirmBooking.totalPrice = self.totalPrice
        }
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}

//
//  ConfirmbookingVC.swift
//  DMRC
//
//  Created by Khushboo Almaula on 27/11/20.
//  Copyright © 2020 khushboo. All rights reserved.
//

import UIKit

class ConfirmbookingVC: UIViewController {
    
    //MARK: Variables and Outlets
    var _activityImage: UIImage?
    @IBOutlet weak var lblDate: UILabel!
    @IBOutlet weak var lblTicketPrice: UILabel!
    @IBOutlet weak var invoiceView:UIView!
    var arrSelectedStation = [[String:Any]]()
    var totalPrice = 0.0
    
    //MARK: View Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        displayCurrentdate()
        // Do any additional setup after loading the view.
    }
    
    //MARK: Custom methods
    func displayCurrentdate(){
        let date = Date()
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd/MM/yyyy"
        let result = dateFormatter.string(from: date)
        self.lblDate.text = result
        self.lblTicketPrice.text = String(totalPrice)
    }
    
    func shareActivityVC(){
        let postPhrase = "DMRC booking confirmation"
        
        //Generating the screenshot
         //Create the UIImage
         UIGraphicsBeginImageContext(invoiceView.frame.size)
         invoiceView.layer.render(in: UIGraphicsGetCurrentContext()!)
         let postImage = UIGraphicsGetImageFromCurrentImageContext()
         UIGraphicsEndImageContext()

        
        let activityViewController : UIActivityViewController = UIActivityViewController(activityItems: [postPhrase, postImage!], applicationActivities: nil)
        
        self.present(activityViewController, animated: true, completion: nil)
    }
    
    //MARK: IBAction Method
    @IBAction func btnActionConfirmBooking(_sender: Any){
        self.performSegue(withIdentifier: "MapSegue", sender: self)
    }
    @IBAction func btnShareInvoice(_sender:Any){
        self.shareActivityVC()
    }
    @IBAction func btnActionback(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "MapSegue" {
            let mapVC = segue.destination as! MapVC
            mapVC.arrJourney = self.arrSelectedStation
        }
    }
}


/*
 // MARK: - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
 // Get the new view controller using segue.destination.
 // Pass the selected object to the new view controller.
 }
 */



//
//  Constant.swift
//  DMRC
//
//  Created by Khushboo Almaula on 28/11/20.
//  Copyright © 2020 khushboo. All rights reserved.
//

import Foundation

struct JourneyDetail{
    
    static let arrJourneyDetail: [[String:Any]] = [
        ["stationId": 0,
         "stationName": "Karol Bagh",
         "isInterchange": false,"subStations":[[]],
         "totalPrice": 2.00,"subStationID":0,"lat":28.6550,"long":77.1888],
        ["stationId": 1,
         "stationName": "Jhande Walan",
         "isInterchange": false,
         "totalPrice": 2.00,"subStations":[[]],"subStationID":0,"lat":28.6473,"long":77.2028],
        ["stationId": 2,
         "stationName": "R.K.Aashram",
         "isInterchange": false,
         "totalPrice": 2.00,"subStations":[[]],"subStationID":0,"lat":28.6392,"long":77.2086],
        ["stationId": 3,
         "stationName": "Rajiv chowk",
         "isInterchange": true,
         "totalPrice": 5.00,
         "subStations":[["stationId": 3,
                         "stationName": "Patel Chowk",
                         "isInterchange": false,
                         "totalPrice": 2.00,"subStationID":1,"lat":28.6233,"long":77.2145],
                        ["stationId": 3,
                         "stationName": "INA",
                         "isInterchange": false,
                         "totalPrice": 2.00,"subStationID":2,"lat":28.5745,"long":77.2095],
                        ["stationId": 3,
                         "stationName": "New delhi",
                         "isInterchange": false,
                         "totalPrice": 2.00,"subStationID":-1,"lat":28.6427,"long":77.2199]],
         "lat":28.6330,"long":77.2194],
        ["stationId": 4,
         "stationName": "Barakh Amba",
         "isInterchange": false,
         "totalPrice": 2.00,"subStations":[[]],"subStationID":0,"lat":28.6299,"long":77.2242],
        ["stationId": 5,
         "stationName": "Mandi House",
         "isInterchange": true,"subStations":[["stationId": 5,
                                               "stationName": "Okhla",
                                               "isInterchange": false,
                                               "totalPrice": 2.00,"subStationID":1,"lat":28.5429,"long":77.2752]],
         "totalPrice": 5.00,"subStationID":0,"lat":28.6259,"long":77.2343]
    ]
}

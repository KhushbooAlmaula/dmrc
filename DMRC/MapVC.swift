//
//  MapVC.swift
//  DMRC
//
//  Created by Khushboo Almaula on 27/11/20.
//  Copyright © 2020 khushboo. All rights reserved.
//

import UIKit
import MapKit

class MapVC: UIViewController, MKMapViewDelegate {
    
    //MARK: Variables and Outlet
    @IBOutlet weak var myMap: MKMapView!
    var myRoute : MKRoute!
    var arrJourney = [[String:Any]]() 
    
    
    //MARK: ViewLife Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        print("arrayy is \(arrJourney)")
        
    }
    
    //MARK: Custom Methods
    func setMapData(){
        let point1 = MKPointAnnotation()
        let point2 = MKPointAnnotation()
        
        point1.coordinate = CLLocationCoordinate2DMake(arrJourney[0]["lat"] as? Double ?? 28.655  , arrJourney[0]["long"] as? Double ?? 77.655)
        point1.title = arrJourney[0]["stationName"] as? String
        myMap.addAnnotation(point1)
        
        point2.coordinate = CLLocationCoordinate2DMake(arrJourney[1]["lat"] as? Double ?? 28.655  , arrJourney[0]["long"] as? Double ?? 77.655)
        point1.title = arrJourney[1]["stationName"] as? String
        
        myMap.addAnnotation(point2)
        myMap.centerCoordinate = point2.coordinate
        myMap.delegate = self
        
        //Span of the map
        myMap.setRegion(MKCoordinateRegion(center: point2.coordinate, span: MKCoordinateSpan(latitudeDelta: 0.7,longitudeDelta: 0.7)), animated: true)
        
        let directionsRequest = MKDirections.Request()
        let markTaipei = MKPlacemark(coordinate: CLLocationCoordinate2DMake(point1.coordinate.latitude, point1.coordinate.longitude), addressDictionary: nil)
        let markChungli = MKPlacemark(coordinate: CLLocationCoordinate2DMake(point2.coordinate.latitude, point2.coordinate.longitude), addressDictionary: nil)
        
        directionsRequest.source = MKMapItem(placemark: markChungli)
        directionsRequest.destination = MKMapItem(placemark: markTaipei)
        
        directionsRequest.transportType = MKDirectionsTransportType.automobile
        let directions = MKDirections(request: directionsRequest)
        
        directions.calculate(completionHandler: {
            response, error in
            
            if error == nil {
                self.myRoute = response!.routes[0] as MKRoute
                self.myMap.addOverlay(self.myRoute.polyline)
            }
            
        })
    }
    
    func mapView(_ mapView: MKMapView, rendererFor overlay: MKOverlay) -> MKOverlayRenderer {
        
        let myLineRenderer = MKPolylineRenderer(polyline: myRoute.polyline)
        myLineRenderer.strokeColor = UIColor.red
        myLineRenderer.lineWidth = 3
        return myLineRenderer
    }
    
    //MARK: IBAction methods
    @IBAction func btnActionback(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}
